import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false

// global directive defining
// defining directive

// Vue.directive('awesome',{
//   bind(el,binding){

//     el.innerHTML = binding.value;
//     el.style.fontSize = '40px';

//     //  if( binding.arg == 'red'){
//     //    el.style.color = 'red';
//     //  } else {
//     //    el.style.color = 'green';
//     //  }

//     el.style.color = binding.modifiers.red ? 'red' : 'blue';
//     el.style.fontSize = binding.modifiers.small ? "14px" : "50px";

//     console.log(binding);
//   }
// })

new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app')
